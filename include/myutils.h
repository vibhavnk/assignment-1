#ifndef __MYUTILS_H
#define __MYUTILS_H
int isPalindrome(int n);
int isPrime(int n);
int factorial(int n);
int sum(int a, ...);
#endif