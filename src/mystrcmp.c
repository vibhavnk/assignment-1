int mystrcmp(char *ptr1,char *ptr2)
{
    while (*ptr1==*ptr2)
        {
            if (*ptr1=='\0'||*ptr2=='\0')
                break;

            ptr1++;
            ptr2++;
        }

    if (*ptr1=='\0'&&*ptr2=='\0')
       return 1;
    else
       return 0;

}