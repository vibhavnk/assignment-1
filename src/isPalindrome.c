int isPalindrome(int n){
    int reversedN = 0, remainder, originalN;
    originalN = n;

    while (n != 0) {
        remainder = n % 10;
        reversedN = reversedN * 10 + remainder;
        n /= 10;
    }

    if (originalN == reversedN)
        return 1;
    else
        return 0;
}