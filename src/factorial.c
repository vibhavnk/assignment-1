#include<stdio.h>
int factorial(int n){
    long fact=1;
    int i;
    if (n < 0)
        return 0;
    
    else {
        for (i = 1; i <= n; i++) {
            fact *= i;
        }
        return fact;
    }
}