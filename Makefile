TARGET=all.out
OBJS=test.o src/factorial.o src/isPalindrome.o src/isPrime.o src/vsum.o src/mystrlen.o src/mystrcat.o src/mystrcpy.o src/mystrcmp.o src/set.o src/reset.o src/flip.o src/query.o


all:$(TARGET)

$(TARGET):$(OBJS)
	gcc $^ -o $@

%.o:%.c include/mystring.h include/myutils.h include/bitmask.h
	gcc $< -c -g



clean:
	rm -rf *.o ${TARGET}

